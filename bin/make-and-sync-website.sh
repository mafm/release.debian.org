#!/bin/sh

set -e

make -C ~release all

# For syncing hints
~release/britney/code/b1/britney updatehtml

if ! static-update-component release.debian.org 2>&1 ; then
    echo "static-update-component release.d.o failed" >&2
    exit 1
fi
