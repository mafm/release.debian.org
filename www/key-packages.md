#! TITLE: On the topic of key packages and autoremoval
#! SUBTITLE: What are they and how are they used

Key packages, what are they?
============================

In order to run a (minimal) Debian system, there is a set of package
that need to be available in a suite. Because we want ourselves and
our users to be able to (re)build the software we ship in a given
suite, build dependencies are needed in that suite too. Additionally,
because we want to be able to install Debian, we also need the
installer and its build-dependencies.

Given the above, there's [a
script](https://salsa.debian.org/qa/udd/-/blob/master/scripts/update-key-packages.pl)
(that runs as part of udd) that will calculate [the current list of
key
packages](https://udd.debian.org/cgi-bin/key_packages.yaml.cgi). The
script starts with all the binary packages of a given priority
(`standard`, `important` and `required`) and a couple of hardcoded
source packages and it recursively finds the corresponding source
packages, the build-dependencies, the packages build by the source and
the dependencies of those two cases until no new packages are found.


How the list of key packages is used
====================================

Obviously having a list alone is nice but doesn't bring us
anywhere. So, the Release Team uses this list to influence the
[autoremoval
calculation](https://salsa.debian.org/qa/udd/-/blob/master/udd/testing_autoremovals_gatherer.pl)
and [autoremoval
hinting](https://salsa.debian.org/release-team/release-tools/-/blob/master/scripts/make_autoremoval_hints.py)
of source packages with Release Critical (RC) bugs. The autoremovals
are serving two purposes. One, the state of `testing` is improving
from the point of number of RC bugs. But more importantly, it creates
a stronger incentive to solve RC bugs (by the maintainer or
maintainers of reverse dependencies). As explained above, autoremoval
doesn't work for key packages as it would leave us with a suite that
doesn't work (or doesn't meet one of our core values), hence, we use
the list to filter key package out from the autoremoval process by
default.
