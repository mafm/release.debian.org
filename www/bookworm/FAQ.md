#! TITLE: bookworm moreinfo questions and reject reasons
#! SUBTITLE: How do I improve my unblock request?

Intro
=====

To avoid that we need to repeat ourselves, we are collecting our standard
requests for info and our reasons for rejects on this page.

Be reminded that the release team consists of humans. We don't like turning
down a request for an unblock. So please consider carefully if the unblock is
really warranted and don't just "try".

During the freeze, there are a lot of unblock requests that need our
attention. Every exception, unrelated change, special case, need for extra
input, etc increases the "cost" of your request on the release team, and
reduces the time for other changes. Following the freeze policy and the
suggestions in this document greatly increase the chances of success, even if
you don't personally agree with these guidelines.


Look at the excuses
===================

A package that is unblocked cannot migrate to testing if there are other
issues preventing the migration. So in that case, you should fix these issues
first and ask for an unblock after that.

The most up-to-date place to see exuses is
[update_excuses](https://release.debian.org/britney/update_excuses.html), but
that's a long list for all
packages. [qa.d.o](https://qa.debian.org/excuses.php) is normally only 15
minutes behind.


Moreinfo
========

I want to add a new upstream release, is that possible?
-------------------------------------------------------

Upstream releases are in general not acceptable during the freeze. However,
every rule has exceptions. Please don't take this path lightly.

During the freeze we only want targeted fixes for bugs of severity important
and higher (according to the BTS definition). It is possible that a new
upstream release *is* a targeted bug fix release. However, the maintainer has
to do the checking. They should convince themselves 100%, so they can convince
us. Questions to ask and answer are:

1. Is this a targeted bug fix release, and how does that show?

1. What are the risks of the changes for the quality of the Debian release?

1. Is there a policy that describes what upstream considers acceptable for this
upstream release?

1. Does that policy align with our bug severity important or higher?

1. Does upstream test thoroughly?

1. Has this package seen new upstream version uploads to stable in the past to
facilitate security support?

1. Look at the diff. If it's long (TODO should we put a number here?), you
probably need a targeted fix.

1. Look at the diff. If there's a number of changes not relevant for Debian,
you probably need a targeted fix.

1. Look at the diff. If there something in there that is difficult to explain,
but not directly related to the (RC or important) bugs you are fixing, you
probably need a targeted fix.

1. Etc...

### Do I need to create bugs in the Debian BTS for the issues?

We care for bugs that are reported in the Debian BTS, but that doesn't mean
that it's all we care about. If the maintainer evaluates the changes done by
upstream and convinced themselves that they all qualified, they may be able to
convince us, but it needs explaining.


How to proceed if my changelog doesn't explain everything?
----------------------------------------------------------

During the freeze, please mention *all* changes to the package in the
debian/changelog. If you don't, then please be verbose in your request for an
unblock. The release team members can't read minds and normally a lot of
requests need to be processed. The more relevant information is put directly
into the initial request, without the need to follow all kind of links to all
kind of places, the easier it is to process as much requests as possible.


What should I do if my unblock requests doesn't get a response in a long time?
------------------------------------------------------------------------------

It happens that your unblock request is met with silence. Typically this means
that the review is difficult, e.g. because of a large amount of changes or
because the unblock should actually be rejected. Consider checking if your
unblock request is following our guidelines. If there is information you can
provide that makes the review easier, e.g. by explaining the rational of certain
changes, chopping changes into logical pieces, or pointing at upstream release
policy, don't hesitate to add that in a follow-up to the unblock bug. Maybe the
unblock should be rejected, but nobody wanted to do that yet (rejecting a
request isn't nice to do), consider closing the unblock request yourself.


What not to do - why you might get a reject
===========================================

Why are unrelated changes not allowed?
--------------------------------------

Every change to code can have unintended side affects. To avoid
introducing new issues, we want targeted fixes during the
freeze. Every unrelated change increases the risk of regressions and
thus raises the bar for review. So, don't add unrelated changes to
your upload and unblock request.


Why can't I add or remove a binary package?
-------------------------------------------

TODO Don't add or remove binary packages without pre-approval.


Causing problems in reverse dependencies
----------------------------------------

If you change requires fixes in reverse dependencies, don't upload it without
pre-approval. Your change is likely to be rejected.


How to proceed if a new upstream version is unreviewable or has not-important changes?
--------------------------------------------------------------------------------------

Upstream releases are not acceptable during the freeze, but see about the
possible exceptions in the moreinfo section. If it's clear that the new
upstream release doesn't qualify for such an exception, an unblock request will
be rejected.

If the new upstream release includes fixes for bugs of severity important and
higher (according to the BTS definition), which you want bring to testing,
you'll have to backport them to the version of your package currently in
testing. You'll have to "undo" your new upstream release upload to unstable by
mangling the version number, e.g. by using the +really style:
`new-upstream-version`+really`upstream-version-in-testing`-1, upload the
package to unstable and request an unblock for that.


I bumped the debhelper compat level, why is it rejected?
--------------------------------------------------------

The debhelper compat mechanism is there to stabilize debhelpers
behaviour. Bumping it is not allowed during the freeze. Changing the debhelper
compat version changes the resulting packages, sometimes in complicated or
unintended ways. Carefully checking the details of these changes is too much
work when processing an unblock request.

